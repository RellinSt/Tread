import os, sys, time
import RPi.GPIO as GPIO
 
def aktuelleTemperatur():
      
    # 1-wire Slave Datei lesen
    file = open('/sys/bus/w1/devices/28-031670ea9eff/w1_slave')
    filecontent = file.read()
    file.close()
 
    # Temperaturwerte auslesen und konvertieren
    stringvalue = filecontent.split("\n")[1].split(" ")[9]
    temperature = float(stringvalue[2:]) / 1000
 
    # Temperatur ausgeben
    rueckgabewert = '%6.2f' % temperature 
    return(rueckgabewert)
 
schleifenZaehler = 0
schleifenAnzahl = 5
schleifenPause = 1
GPIO.setmode(GPIO.BCM) # nicht Board, sondern GPIO-Nr.
 
print "Temperaturabfrage fuer ", schleifenAnzahl,
" Messungen alle ", schleifenPause ," Sekunden gestartet"
 
while schleifenZaehler <= schleifenAnzahl:
    #messdaten = aktuelleTemperatur()
    #print "Aktuelle Temperatur : ", messdaten, "C",
    #"in der ", schleifenZaehler, ". Messabfrage"
    print aktuelleTemperatur()
    time.sleep(schleifenPause)
    schleifenZaehler = schleifenZaehler + 1
    
 
print "Temperaturabfrage beendet"


for i in range(20):
    GPIO.output(i, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(i, GPIO.Low)


